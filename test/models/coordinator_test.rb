require 'test_helper'

class CoordinatorTest < ActiveSupport::TestCase
  should validate_uniqueness_of(:email)
  should validate_presence_of(:email)
  should have_and_belong_to_many(:registries)
  should have_many :enrollments
  should have_many(:participants).through(:enrollments)

  test "CSV.generate" do
expected_csv1 =
"Coordinator_Name,Email,Phone,Participant_Name,Gender,DOB
coordinator 2,email_2@coordinator.com,2999999999,Participant 2,Female,1996-11-11
coordinator 1,email_1@coordinator.com,1999999999,Participant 1,Male,2006-11-11
"
expected_csv2 =
"Coordinator_Name,Email,Phone,Participant_Name,Gender,DOB
coordinator 2,email_2@coordinator.com,2999999999,Participant 2,Female,1996-11-11
coordinator 2,email_2@coordinator.com,2999999999,Particpant 3,Male,2016-11-11
coordinator 1,email_1@coordinator.com,1999999999,Participant 1,Male,2006-11-11
"
    assert_equal Coordinator.to_csv, expected_csv1
    Coordinator.find_by(email: "email_2@coordinator.com").participants << [Participant.new(name: "Particpant 3", gender: "Male", dob: Date.today)]
    assert_equal Coordinator.to_csv, expected_csv2
  end
end