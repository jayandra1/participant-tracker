require 'test_helper'

class EnrollmentTest < ActiveSupport::TestCase
  should validate_uniqueness_of(:participant_id).scoped_to(:registry_id).with_message("participant can't be enrolled in the same registry more than once")

  should belong_to(:registry)
  should belong_to(:participant)
  should belong_to(:coordinator)

  def test_constants
    assert_equal Enrollment::METHOD_OF_CONTACT, [:phone, :email]
  end
end
