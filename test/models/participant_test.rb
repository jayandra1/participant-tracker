require 'test_helper'

class ParticipantTest < ActiveSupport::TestCase
  should have_many :enrollments
end
