require 'test_helper'

class RegistryTest < ActiveSupport::TestCase
  should have_and_belong_to_many(:coordinators)
  should have_many :enrollments
  should have_many(:participants).through(:enrollments)
  should validate_uniqueness_of(:name).scoped_to(:location).with_message('combination of name and location must be unique')
  should validate_uniqueness_of(:location).scoped_to(:name).with_message('combination of name and location must be unique')

  def test_scopes
    assert_equal Registry.open.where_values_hash, {"state" => true}
  end
end