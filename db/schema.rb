# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161110213353) do

  create_table "coordinators", force: :cascade do |t|
    t.string   "email",      limit: 255
    t.string   "name",       limit: 255
    t.string   "phone",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "coordinators_registries", id: false, force: :cascade do |t|
    t.integer "coordinator_id", limit: 4, null: false
    t.integer "registry_id",    limit: 4, null: false
  end

  add_index "coordinators_registries", ["coordinator_id", "registry_id"], name: "index_coordinators_registries_on_coordinator_id_and_registry_id", using: :btree
  add_index "coordinators_registries", ["registry_id", "coordinator_id"], name: "index_coordinators_registries_on_registry_id_and_coordinator_id", using: :btree

  create_table "enrollments", force: :cascade do |t|
    t.string   "method_of_contact",  limit: 255
    t.text     "remarks",            limit: 65535
    t.date     "date_of_enrollment"
    t.integer  "registry_id",        limit: 4
    t.integer  "participant_id",     limit: 4
    t.integer  "coordinator_id",     limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "enrollments", ["coordinator_id"], name: "index_enrollments_on_coordinator_id", using: :btree
  add_index "enrollments", ["participant_id"], name: "index_enrollments_on_participant_id", using: :btree
  add_index "enrollments", ["registry_id"], name: "index_enrollments_on_registry_id", using: :btree

  create_table "participants", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "gender",     limit: 255
    t.date     "dob"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "registries", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "location",   limit: 255
    t.boolean  "state"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

end
