class CreateJoinTableCoordinateRegistry < ActiveRecord::Migration
  def change
    create_join_table :coordinators, :registries do |t|
      t.index [:coordinator_id, :registry_id]
      t.index [:registry_id, :coordinator_id]
    end
  end
end
