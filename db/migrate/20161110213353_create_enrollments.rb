class CreateEnrollments < ActiveRecord::Migration
  def change
    create_table :enrollments do |t|
      t.string :method_of_contact
      t.text :remarks
      t.date :date_of_enrollment
      t.references :registry, index: true, foreign_key: false
      t.references :participant, index: true, foreign_key: false
      t.references :coordinator, index: true, foreign_key: false

      t.timestamps null: false
    end
  end
end
