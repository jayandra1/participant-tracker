# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


5.times do |i|
  Coordinator.create(email: "email#{i}@coordinator.com", name: "coordinator-#{i}", phone: "#{i}999999999")
  Registry.create(name: "registry-#{i}", location: "location-#{i}", state: true)
  Participant.create(name: "participant-#{i}", gender: "male", dob: Date.today-i.years)
end

registry_0 = Registry.find_by(name: "registry-0")
registry_1 = Registry.find_by(name: "registry-1")
registry_2 = Registry.find_by(name: "registry-2")
coordinator_0 = Coordinator.find_by(email: "email0@coordinator.com")
coordinator_1 = Coordinator.find_by(email: "email1@coordinator.com")
coordinator_2 = Coordinator.find_by(email: "email2@coordinator.com")
coordinator_3 = Coordinator.find_by(email: "email3@coordinator.com")
participant_0 = Participant.find_by(name: "participant-0")
participant_1 = Participant.find_by(name: "participant-1")
participant_2 = Participant.find_by(name: "participant-2")
participant_3 = Participant.find_by(name: "participant-3")
participant_4 = Participant.find_by(name: "participant-4")


registry_0.coordinators << coordinator_0
registry_0.coordinators << coordinator_1
registry_0.coordinators << coordinator_2

registry_1.coordinators << coordinator_1
registry_1.coordinators << coordinator_2

registry_2.coordinators << coordinator_2
registry_2.coordinators << coordinator_0

Enrollment.create(registry: registry_0, participant: participant_0, coordinator: coordinator_0)
Enrollment.create(registry: registry_0, participant: participant_1, coordinator: coordinator_1)
Enrollment.create(registry: registry_0, participant: participant_4, coordinator: coordinator_1)
Enrollment.create(registry: registry_0, participant: participant_2, coordinator: coordinator_2)
Enrollment.create(registry: registry_0, participant: participant_3, coordinator: coordinator_3)

Enrollment.create(registry: registry_1, participant: participant_0, coordinator: coordinator_0)
Enrollment.create(registry: registry_1, participant: participant_1, coordinator: coordinator_1)
Enrollment.create(registry: registry_1, participant: participant_2, coordinator: coordinator_2)

Enrollment.create(registry: registry_2, participant: participant_0, coordinator: coordinator_0)
Enrollment.create(registry: registry_2, participant: participant_1, coordinator: coordinator_1)