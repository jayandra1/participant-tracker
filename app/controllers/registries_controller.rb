class RegistriesController < ApplicationController
  before_action :set_registry, only: [:show, :edit, :update, :destroy, :remove_coordinator]
  before_action :build_options_for_selects, only: [:new, :edit, :create, :update]

  # GET /registries
  # GET /registries.json
  def index
    @registries = Registry.all
  end

  # GET /registries/1
  # GET /registries/1.json
  def show
    @coordinators = @registry.coordinators.includes(:participants)
  end

  # GET /registries/new
  def new
    @registry = Registry.new
  end

  # GET /registries/1/edit
  def edit
  end

  # POST /registries
  # POST /registries.json
  def create
    @registry = Registry.new(registry_params)

    respond_to do |format|
      if @registry.save
        format.html { redirect_to @registry, notice: 'Registry was successfully created.' }
        format.json { render :show, status: :created, location: @registry }
      else
        format.html { render :new }
        format.json { render json: @registry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /registries/1
  # PATCH/PUT /registries/1.json
  def update
    respond_to do |format|
      if @registry.update(registry_params)
        format.html { redirect_to @registry, notice: 'Registry was successfully updated.' }
        format.json { render :show, status: :ok, location: @registry }
      else
        format.html { render :edit }
        format.json { render json: @registry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /registries/1
  # DELETE /registries/1.json
  def destroy
    @registry.destroy
    respond_to do |format|
      format.html { redirect_to registries_url, notice: 'Registry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def remove_coordinator
    coordinator = Coordinator.where(id: params[:coordinator_id]).first

    respond_to do |format|
      if coordinator && @registry.coordinators.include?(coordinator)
        @registry.coordinators.delete(coordinator)
        format.html { redirect_to @registry, notice: 'Coordinator was successfully removed.' }
        format.json { render plain: "OK", status: :ok }
      else
        format.html { redirect_to @registry, notice: 'Something went wrong !!!' }
        format.json { render plain: "Error", status: 400 }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_registry
      @registry = Registry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def registry_params
      params.require(:registry).permit(:name, :location, :state, coordinator_ids:[])
    end

    def build_options_for_selects
      @coordinators = Coordinator.all
    end
end
