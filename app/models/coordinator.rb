class Coordinator < ActiveRecord::Base
  require 'csv'

  validates_uniqueness_of :email
  validates_presence_of :email

  has_and_belongs_to_many :registries, -> { distinct }
  has_many :enrollments
  has_many :participants, through: :enrollments

  def self.to_csv
    coordinator_header = %w{Coordinator_Name Email Phone}
    participant_header = %w{Participant_Name Gender DOB}
    coordinator_attributes = %w{name email phone}
    participant_attributes = %w{name gender dob}

    CSV.generate(headers: true) do |csv|
      csv << [coordinator_header, participant_header].flatten

      all.each do |c|
        coordinator_values = coordinator_attributes.map{ |attr| c.send(attr) }
        c.participants.uniq.each do |p|
          participant_values = participant_attributes.map{ |attr| p.send(attr) }
          csv << [coordinator_values, participant_values].flatten
        end
      end
    end
  end
end
