class Enrollment < ActiveRecord::Base
  validates_uniqueness_of :participant_id, scope: :registry_id, message: "participant can't be enrolled in the same registry more than once"

  belongs_to :registry
  belongs_to :participant
  belongs_to :coordinator

  METHOD_OF_CONTACT = [:phone, :email]
end
