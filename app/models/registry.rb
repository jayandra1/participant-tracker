class Registry < ActiveRecord::Base
  validates_uniqueness_of :name, scope: :location, message: "combination of name and location must be unique"
  validates_uniqueness_of :location, scope: :name, message: "combination of name and location must be unique"

  has_and_belongs_to_many :coordinators, -> { distinct }
  has_many :enrollments
  has_many :participants, through: :enrollments

  scope :open, -> {where(state: true)}
end