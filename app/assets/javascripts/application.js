// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require_tree .
$( document ).ready(function() {
    $("div.alert a.close").click(function(){
        $(this).parents("div.alert").remove();
    });

    $('a.remove_associations').bind('ajax:complete', function(event, xhr, settings) {
        if(xhr.status == 200){
            var current_tr = event.currentTarget.parentNode.parentNode;
            //$(current_tr).next("tr.associated_tr").remove();
            current_tr.remove();
        }
        if(xhr.status == 400){
            alert("Something Went Wrong !!!")
        }
    });
});