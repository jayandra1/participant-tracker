json.extract! participant, :id, :name, :gender, :dob, :created_at, :updated_at
json.url participant_url(participant, format: :json)